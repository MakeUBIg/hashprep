
    window.smartlook||(function(d) {
    var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
    var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
    c.charset='utf-8';c.src='https://rec.smartlook.com/recorder.js';h.appendChild(c);
    })(document);
    smartlook('init', 'a991965d6cdfa6eff3d806aad3cbbc4b98b815a2');

	var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        slidesPerView: 3,
        centeredSlides: true,
        paginationClickable: true,
        loop: true,
        breakpoints: {
            768: {
                slidesPerView: 1
            }
        }
    });
    	 $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        if (scroll > '20') {
            $('.navbar-default').addClass('nav-white');
        }
        else if (scroll < '20') {
            $('.navbar-default').removeClass('nav-white');
        }
    });
    $( document ).ready(function() {
        var scroll = $(window).scrollTop();
        if (scroll > '20') {
            $('.navbar-default').addClass('nav-white');
        }
        else if (scroll < '20') {
            $('.navbar-default').removeClass('nav-white');
        }
    });
    $('.animated').each(function() {
        $(this).appear(function(){

            var element = $(this);

            var animation = element.attr('data-animation');
            if(!element.hasClass('visible')){
                var animationDelay = element.attr('data-animation-delay');
                setTimeout(function(){
                            element.addClass(animation+' visible');
                        }, animationDelay
                );
            }
        });

    });

     $('body').scrollspy({
        target: '#nav-main',
        offset: 65
    });

    $(function() {
        $('a.page-scroll').bind('click', function(event) {
            if($(window).width < 768)
            {
                var $anchor = $(this);
                $('html, body').stop().animate({
                    scrollTop: $($anchor.attr('href')).offset().top - 50
                }, 1500, 'easeInOutExpo');
                event.preventDefault();
            }
            else
            {
                var $anchor = $(this);
                $('html, body').stop().animate({
                    scrollTop: $($anchor.attr('href')).offset().top - 65
                }, 1500, 'easeInOutExpo');
                event.preventDefault();
            }

        });
    });
    
	$("#hashform").validate({
        errorClass: "has-error",
        validClass: "has-success",
		rules: {
			email: {
				required: true,
				email: true
			},
			mobile: {
				required: true,
				number: true,
                minlength: 10,
                maxlength: 13
			},
			name: {
                required: true,
               
            },
		messages: {
			name: "Please enter your name",
			email: "Please enter a valid email address",
			mobile: "Please enter mobile number"
            }
        },
		submitHandler : function(form) {
    	var form = $('#hashform').serialize();
		$.ajax({
         type:'POST',
         url:"ZohoConnector.php",
         data:form,
        success: function(result)
        {
        	$('#hashform').html("<center><h3 style='color:white;''>Thank you for Contacting Us.</h3><br /><h5 style='color: white;'>We will Soon get in touch with you!</h5></center>");
        }
    	});
	}

});
    $(function(){
  $('#hashform').one('click', function() {  
    $(this).attr('disabled','disabled');
  });
});
