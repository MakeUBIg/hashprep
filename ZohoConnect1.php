<?php

include './ArrayToXML.php';

class ZohoConnector
{
    public function __construct()
    {
        header('Content-Type: application/xml');
        $this->sendData();
        
    }

    public function sendData()
    {
        $converter = new ArrayToXML();
        $Leads= array();
        $Leads['row']['@no'] = '1';
        $Leads['row']['FL'][] = ['@val' => 'Lead Source','%' => 'Web Download'];
        $Leads['row']['FL'][] = ['@val' => 'Company','%' => 'MakeUBIg'];
        $Leads['row']['FL'][] = ['@val' => 'First Name','%' => 'firstname'];
        $Leads['row']['FL'][] = ['@val' => 'Last Name','%' => 'lastname'];
        $Leads['row']['FL'][] = ['@val' => 'Email','%' => 'admin@makeubig.com'];
        $Leads['row']['FL'][] = ['@val' => 'Title','%' => 'coihfoihf'];
        $Leads['row']['FL'][] = ['@val' => 'Phone','%' => '40874097494'];
        $Leads['row']['FL'][] = ['@val' => 'Home Phone','%' => '34874097409'];
        $Leads['row']['FL'][] = ['@val' => 'Mobile','%' => '09873873683'];
        $Leads['row']['FL'][] = ['@val' => 'Description','%' => 'fpof onf oibfooi oibof oiboibf oiboibfobfoih foibofb'];
        $Leads['row']['FL'][] = ['@val' => 'Lead Status','%' => 'Not Contacted'];
        $param = $converter->buildXML($Leads);
        // echo $param;die;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://crm.zoho.com/crm/private/xml/Leads/insertRecords");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,"authtoken=bb85d44bfc87e4356424ec3a18f22cc1&scope=crmapi&xmlData=".$param);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec ($ch);
        curl_close ($ch);
        $this->displayResponse($server_output);
    }

    public function displayResponse($server_output)
    {
        header('Content-Type: text/html; charset=utf-8');
        die(print_r($server_output));
        echo 'server responded as'.$server_output;
    }
}
$initialized = new ZohoConnector();
